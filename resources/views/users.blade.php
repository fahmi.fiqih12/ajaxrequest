<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div id="App">
        <form>
            <input type="text" v-model="form.name">
            <button @click.prevent="addUser" v-show="!updateSubmit">Add</button>
            <button @click.prevent="update" v-show="updateSubmit">Update</button>
        </form>

        <ul>
            <li v-for="(user, index) in users">
                <span>@{{user.name}}</span>
                <button @click.prevent="edit(user, index)">edit</button>
                <button @click.prevent="del(index, user)">delete</button>
            </li>
        </ul>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
        new Vue({
            el : '#App',
            data:{
                form : {
                    'name' : ''
                },
                updateSubmit : false,
                selectedUserId : null,
                users : []
            },
            methods:{
                addUser(){
                    // POST /someUrl
                    this.$http.post('/api/user', {name: this.form}).then(response => {
                        this.users.push(
                            this.form)
                        this.form = {}
                    });

                   
                },
                edit(user, index){
                    this.selectedUserId = index
                    this.updateSubmit = true
                    this.form.name = user.name
                },
                update(){
                    this.$http.post('/api/user/update-user' + user.id ).then(response => {
                        this.users[this.selectedUserId].name = this.form.name
                        this.form = {}
                        this.updateSubmit = false
                        this.selectedUserId = null
                    });
                },
                del(index, user){
                    this.$http.post('/api/user/delete' + user.id ).then(response => {
                        var konfirm = confirm('Anda Yakin')
                        if(konfirm){
                            this.users.splice(index, 1)
                        }
                    });
                    
                }

            },
            mounted:function() {
                // GET /someUrl
                this.$http.get('/api/user').then(response => {
                    let result = response.body.data
                    this.users = result
                });
            }
        });
    </script>

</body>
</html>